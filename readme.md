## Test Webboard by Nui

To build and run :

1. Run $ `gradlew bootJar` in /backend/testwebboard  
a jar file `backend/testwebboard/build/libs/testwebboard-0.1.1-SNAPSHOT.jar` will be created.

2. Create a properties file e.g. `app.properties` somewhere with contents like

    app.jwtSecret=`JWTSuperSecretKey`  
    app.jwtExpirationInMs=`3600000`  
    app.disable-default-exception-handling=`true`  
      
    spring.jpa.hibernate.ddl-auto=`validate`  
    spring.datasource.url=`jdbc:mysql://localhost:3306/testwebboard?serverTimezone=UTC`  
    spring.datasource.username=`testwebboard`  
    spring.datasource.password=`password`  

3. Run the jar file with command `start` and `--spring.config.location=path/to/app.properties`

    $ `cd backend/testwebboard/build/libs`  
    $ `./testwebboard-0.1.1-SNAPSHOT.jar start --spring.config.location=app.properties`  

To generate database :

1. Config `spring.jpa.hibernate.ddl-auto` to `create`
2. Run the app once
3. Reset `spring.jpa.hibernate.ddl-auto` back to `validate`

To test with Postman :

1. Import `TestWebboard.postman_collection.json` and `EC2.postman_environment.json` into Postman app

2. Select `EC2` environment in Postman app

3. Login with `Login` request and the token will be available to other requests automatically
