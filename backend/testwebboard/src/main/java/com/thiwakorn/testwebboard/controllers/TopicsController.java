package com.thiwakorn.testwebboard.controllers;

import com.thiwakorn.testwebboard.data.entities.Topic;
import com.thiwakorn.testwebboard.models.TopicCreateModel;
import com.thiwakorn.testwebboard.models.TopicDetailModel;
import com.thiwakorn.testwebboard.models.TopicListItem;
import com.thiwakorn.testwebboard.security.CurrentUser;
import com.thiwakorn.testwebboard.security.UserPrincipal;
import com.thiwakorn.testwebboard.services.TopicsService;

import java.util.Collection;
import java.util.Optional;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;

@RestController
public class TopicsController {

  @Autowired
  private TopicsService service;

  @GetMapping("/topics")
  public Collection<TopicListItem> getTopics(
      @RequestParam(name = "q", defaultValue = "") String query,
      @RequestParam(name = "perpage", defaultValue = "10") int itemsPerPage,
      @RequestParam(defaultValue = "1") int page) {
    return service.listTopics(page, itemsPerPage, query);
  }

  @PostMapping("/topics")
  public ResponseEntity<Topic> createTopic(
      @CurrentUser UserPrincipal userPrincipal,
      @RequestBody TopicCreateModel model) {
    return ResponseEntity.ok(service.addTopic(userPrincipal, model));
  }

  @GetMapping("/topics/{id}")
  public ResponseEntity<TopicDetailModel> getTopic(
    @PathVariable long id) {
    Optional<Topic> topic = service.getTopic(id);

    if (!topic.isPresent())
      return ResponseEntity.notFound().build();

    return ResponseEntity.ok(new TopicDetailModel(topic.get(), service.getTopicComments(id)));
  }

  @DeleteMapping("/topics/{id}")
  public ResponseEntity<Topic> deleteTopic(
      @CurrentUser UserPrincipal userPrincipal,
      @PathVariable long id) {
    Optional<Topic> deleted = service.deleteTopic(userPrincipal, id);

    if (!deleted.isPresent())
      return ResponseEntity.notFound().build();
        
    return ResponseEntity.ok(deleted.get());
  }

}
