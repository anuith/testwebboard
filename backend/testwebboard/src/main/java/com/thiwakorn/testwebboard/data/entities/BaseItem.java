package com.thiwakorn.testwebboard.data.entities;

import java.time.Instant;
import java.util.Date;

import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@MappedSuperclass
public abstract class BaseItem {

  @ManyToOne
  @JsonIgnore
  private User creator;
  @JsonIgnore
  private String ipAddress;
  @JsonIgnore
  private Instant dateCreate;

  public BaseItem() {
  }

  protected BaseItem(User creator, String ipAddress) {
    this.creator = creator;
    this.ipAddress = ipAddress;
  }

  @JsonGetter
  public String getIpAddress() {
    return ipAddress;
  }

  @JsonGetter
  public String getCreator() {
    return creator.getUsername();
  }

  @JsonProperty
  public Date getDateCreate() {
    return Date.from(dateCreate);
  }

  @PrePersist
  void preInsert() {
    if (this.dateCreate == null)
      this.dateCreate = Instant.now();
  }

}
