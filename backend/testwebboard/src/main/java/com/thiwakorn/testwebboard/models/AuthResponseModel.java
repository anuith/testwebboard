package com.thiwakorn.testwebboard.models;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AuthResponseModel {

  @JsonProperty
  private String accessToken;
  @JsonProperty
  private Date expireDate;

  private String tokenType = "Bearer";

  public AuthResponseModel(String accessToken) {
    this.accessToken = accessToken;
  }
  
  @JsonGetter
  public String getTokenType() {
    return tokenType;
  }
  
}
