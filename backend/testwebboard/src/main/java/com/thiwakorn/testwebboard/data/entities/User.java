package com.thiwakorn.testwebboard.data.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id = 0;
  
  private String username;
  private String passwordHash;


  public User() {
  }

  public User(String username, String passwordHash)
  {
    this.username = username;
    this.passwordHash = passwordHash;
  }


  @JsonIgnore
  public Long getId() {
    return id;
  }

  public String getUsername() {
    return username;
  }

  @JsonIgnore
  public String getPasswordHash() {
    return passwordHash;
  }

}
