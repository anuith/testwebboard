package com.thiwakorn.testwebboard.data.entities;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Entity
@JsonPropertyOrder(value = {"id", "title", "content"})
public class Topic extends BaseItem {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @JsonIgnore
  private long id = 0;

  private String title;
  private String content;

  @OneToMany(mappedBy = "topic", fetch = FetchType.LAZY)
  private List<Comment> comments;

  public Topic() {
  }

  public Topic(User user, String ipAddress, String title, String content) {
    super(user, ipAddress);
    this.title = title;
    this.content = content;
  }

  @JsonProperty
  public long getId() {
    return id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String value) {
    title = value;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String value) {
    content = value;
  }
  
}
