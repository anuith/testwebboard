package com.thiwakorn.testwebboard.controllers;

import com.thiwakorn.testwebboard.data.entities.Comment;
import com.thiwakorn.testwebboard.models.CommentCreateModel;
import com.thiwakorn.testwebboard.security.CurrentUser;
import com.thiwakorn.testwebboard.security.UserPrincipal;
import com.thiwakorn.testwebboard.services.TopicsService;

import java.util.Collection;
import java.util.Optional;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;

@RestController
public class CommentsController {

  @Autowired
  private TopicsService service;

  @GetMapping("/topics/{topicId}/comments")
  public Collection<Comment> getTopicComments(@PathVariable int topicId) {
    return service.getTopicComments(topicId);
  }

  @PostMapping("/topics/{topicId}/comments")
  public ResponseEntity<Comment> addComment(
      @CurrentUser UserPrincipal userPrincipal, @PathVariable int topicId,
      @RequestBody CommentCreateModel model) {

    Optional<Comment> result = service.addTopicComment(userPrincipal, topicId, model);

    if (!result.isPresent())
      return ResponseEntity.notFound().build();

    return ResponseEntity.ok(result.get());
  }

  @DeleteMapping("/comments/{id}")
  public ResponseEntity<Comment> deleteTopic(
      @CurrentUser UserPrincipal userPrincipal,
      @PathVariable int id) {

    Optional<Comment> deleted = service.deleteTopicComment(userPrincipal, id);

    if (!deleted.isPresent())
      return ResponseEntity.notFound().build();

    return ResponseEntity.ok(deleted.get());
  }

}
