package com.thiwakorn.testwebboard.data.repositories;

import com.thiwakorn.testwebboard.data.entities.User;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<User, Long> {

  public Optional<User> findByUsername(String username);

  public boolean existsByUsername(String username);

}
