package com.thiwakorn.testwebboard.models;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class AuthModel {

  @NotBlank
  @Size(min = 3, max = 15)
  private String username;

  @NotBlank
  @Size(min = 6, max = 50)
  private String password;

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }

}
