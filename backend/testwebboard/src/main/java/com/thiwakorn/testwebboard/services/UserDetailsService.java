package com.thiwakorn.testwebboard.services;

import com.thiwakorn.testwebboard.data.entities.User;
import com.thiwakorn.testwebboard.data.repositories.UsersRepository;
import com.thiwakorn.testwebboard.security.UserPrincipal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

  @Autowired
  UsersRepository usersRepository;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    User user = usersRepository.findByUsername(username)
        .orElseThrow(() -> new UsernameNotFoundException(String.format("Username '%s' not found", username)));

    return UserPrincipal.create(user);
  }

  public UserDetails loadUserById(Long id) {
    User user = usersRepository.findById(id)
        .orElseThrow(() -> new UsernameNotFoundException(String.format("User id '' not found with id : ", id)));

    return UserPrincipal.create(user);
  }

}
