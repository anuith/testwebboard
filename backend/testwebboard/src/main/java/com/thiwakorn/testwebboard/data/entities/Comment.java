package com.thiwakorn.testwebboard.data.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Entity
@JsonPropertyOrder(value = {"id", "topicId", "content"})
public class Comment extends BaseItem {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @JsonIgnore
  private long id = 0;

  @ManyToOne
  @JsonIgnore
  private Topic topic;

  private String content;

  public Comment() {
  }

  public Comment(User user, String ipAddress, Topic topic, String content) {
    super(user, ipAddress);
    this.topic = topic;
    this.content = content;
  }

  @JsonGetter("topicId")
  private long getTopicId() {
    return topic.getId();
  }

  @JsonProperty
  public long getId() {
    return id;
  }

  @JsonIgnore
  void setId(long value) {
    id = value;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String value) {
    content = value;
  }
  
}
