package com.thiwakorn.testwebboard.models;

import com.thiwakorn.testwebboard.data.entities.Topic;
import com.thiwakorn.testwebboard.data.entities.User;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TopicCreateModel {

  @JsonProperty
  private String title;
  @JsonProperty
  private String content;

  public Topic createTopicEntity(User creator, String ipAddress) {
    return new Topic(creator, ipAddress, title, content);
  }
  
}
