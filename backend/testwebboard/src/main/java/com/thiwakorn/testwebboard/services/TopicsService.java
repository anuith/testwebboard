package com.thiwakorn.testwebboard.services;

import com.thiwakorn.testwebboard.data.entities.Comment;
import com.thiwakorn.testwebboard.data.entities.Topic;
import com.thiwakorn.testwebboard.data.entities.User;
import com.thiwakorn.testwebboard.data.repositories.CommentsRepository;
import com.thiwakorn.testwebboard.data.repositories.TopicsRepository;
import com.thiwakorn.testwebboard.data.repositories.UsersRepository;
import com.thiwakorn.testwebboard.models.CommentCreateModel;
import com.thiwakorn.testwebboard.models.TopicCreateModel;
import com.thiwakorn.testwebboard.models.TopicListItem;
import com.thiwakorn.testwebboard.security.UserPrincipal;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

@Service
public class TopicsService {

  @Autowired
  private HttpServletRequest request;
  @Autowired
  private UsersRepository usersRepository;
  @Autowired
  private TopicsRepository topicsRepository;
  @Autowired
  private CommentsRepository commentsRepository;

  public List<TopicListItem> listTopics(int page, int itemsPerPage, String query) {
    page = Math.max(page - 1, 0);
    itemsPerPage = Math.max(itemsPerPage, 1);

    Pageable pageable = PageRequest.of(page, itemsPerPage);

    return topicsRepository.findByKeyword(query, pageable).getContent();
  }

  public Optional<Topic> getTopic(long id) {
    return topicsRepository.findById(id);
  }

  public Topic addTopic(UserPrincipal principal, TopicCreateModel model) {
    Optional<User> user = usersRepository.findById(principal.getId());
    Topic topic = model.createTopicEntity(user.get(), request.getRemoteAddr());

    return topicsRepository.save(topic);
  }

  public Optional<Topic> deleteTopic(UserPrincipal principal, long id) {
    Optional<Topic> topic = topicsRepository.findByIdAndCreatorId(id, principal.getId());

    if (!topic.isPresent())
      return Optional.empty();

    topicsRepository.delete(topic.get());
    return topic;
  }

  public Collection<Comment> getTopicComments(long topicId) {
    return commentsRepository.findByTopicId(topicId, Sort.by(Direction.DESC, "dateCreate"));
  }

  public Optional<Comment> addTopicComment(UserPrincipal principal, long topicId, CommentCreateModel model) {
    Optional<Topic> topic = topicsRepository.findById(topicId);

    if (!topic.isPresent())
      return Optional.empty();

    Optional<User> user = usersRepository.findById(principal.getId());
    Comment comment = model.createCommentEntity(user.get(), request.getRemoteAddr(), topic.get());

    return Optional.of(commentsRepository.save(comment));
  }

  public Optional<Comment> deleteTopicComment(UserPrincipal principal, long commentId) {
    Optional<Comment> comment = commentsRepository.findByIdAndCreatorId(commentId, principal.getId());

    if (!comment.isPresent())
      return Optional.empty();

    commentsRepository.delete(comment.get());
    return comment;
  }

}
