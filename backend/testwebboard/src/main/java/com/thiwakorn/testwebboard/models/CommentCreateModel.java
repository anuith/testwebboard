package com.thiwakorn.testwebboard.models;

import com.thiwakorn.testwebboard.data.entities.Comment;
import com.thiwakorn.testwebboard.data.entities.Topic;
import com.thiwakorn.testwebboard.data.entities.User;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CommentCreateModel {

  @JsonProperty
  private String content;

  public Comment createCommentEntity(User creator, String ipAddress, Topic topic) {
    return new Comment(creator, ipAddress, topic, content);
  }
  
}
