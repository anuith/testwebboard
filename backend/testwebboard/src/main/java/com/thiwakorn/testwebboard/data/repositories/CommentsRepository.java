package com.thiwakorn.testwebboard.data.repositories;

import com.thiwakorn.testwebboard.data.entities.Comment;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentsRepository extends JpaRepository<Comment, Long> {

  public List<Comment> findByTopicId(long topicId, Sort sort);

  public Optional<Comment> findByIdAndCreatorId(long commentId, long creatorId);
  
}
