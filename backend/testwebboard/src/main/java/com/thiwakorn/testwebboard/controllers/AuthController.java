package com.thiwakorn.testwebboard.controllers;

import com.thiwakorn.testwebboard.data.entities.User;
import com.thiwakorn.testwebboard.data.repositories.UsersRepository;
import com.thiwakorn.testwebboard.models.AuthModel;
import com.thiwakorn.testwebboard.models.AuthResponseModel;
import com.thiwakorn.testwebboard.security.JwtTokenProvider;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthController {

  @Autowired
  AuthenticationManager authManager;

  @Autowired
  PasswordEncoder passwordEncoder;

  @Autowired
  UsersRepository usersRepository;

  @Autowired
  JwtTokenProvider tokenProvider;

  @PostMapping("/auth")
  public AuthResponseModel auth(@Valid @RequestBody AuthModel model) {
    Authentication authentication = authManager
        .authenticate(new UsernamePasswordAuthenticationToken(
          model.getUsername(),
          model.getPassword()));

    SecurityContextHolder.getContext().setAuthentication(authentication);
    String token = tokenProvider.generateToken(authentication);

    return new AuthResponseModel(token);
  }

  @PostMapping("/register")
  public ResponseEntity<User> registerUser(@Valid @RequestBody AuthModel model) {
    if (usersRepository.existsByUsername(model.getUsername())) {
      return ResponseEntity.badRequest().build();
    }

    String username = model.getUsername();
    String passwordHash = passwordEncoder.encode(model.getPassword());

    User user = usersRepository.save(new User(username, passwordHash));

    return ResponseEntity.ok(user);
  }

}
