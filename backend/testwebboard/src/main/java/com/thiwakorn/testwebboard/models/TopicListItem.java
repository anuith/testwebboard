package com.thiwakorn.testwebboard.models;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.thiwakorn.testwebboard.data.entities.Topic;

@JsonPropertyOrder(value = {"topic"})
public interface TopicListItem {
  @JsonUnwrapped
  Topic getTopic();
  int getComments();
  Date getLastComment();
}
