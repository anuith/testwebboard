package com.thiwakorn.testwebboard.models;

import com.thiwakorn.testwebboard.data.entities.Topic;
import com.thiwakorn.testwebboard.data.entities.Comment;

import java.util.ArrayList;
import java.util.Collection;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TopicDetailModel {

  @JsonProperty
  private Topic topic;
  @JsonProperty
  private Collection<Comment> comments;

  public TopicDetailModel(Topic topic) {
    this.topic = topic;
    this.comments = new ArrayList<Comment>();
  }

  public TopicDetailModel(Topic topic, Collection<Comment> comments) {
    this.topic = topic;
    this.comments = comments;
  }
  
}
