package com.thiwakorn.testwebboard.data.repositories;

import java.util.Optional;

import com.thiwakorn.testwebboard.data.entities.Topic;
import com.thiwakorn.testwebboard.models.TopicListItem;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TopicsRepository extends JpaRepository<Topic, Long> {

  @Query("select t as topic, count(tc) as comments, max(tc.dateCreate) as lastComment " +
         "from Topic t left join t.comments tc " +
         "where t.title like %?1% or t.content like %?1% " +
         "group by t.id " +
         "order by cast(t.dateCreate as date) desc, max(tc.dateCreate) desc ")
  public Page<TopicListItem> findByKeyword(String keyword, Pageable pageable);

  public Optional<Topic> findByIdAndCreatorId(long id, long creatorId);

}
